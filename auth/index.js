
const passport = require('passport');
const User = require('../models/User.model');
const registerStrategy = require('./register.strategy');
const loginStrategy = require('./login.strategy');
const logoutStrategy = require('./logout.strategy');

passport.serializeUser((user, done) => { return done(null, user._id); });
passport.deserializeUser(async (userId, done) => {
    try {

        const existingUser = await User.findById({ userId });
        return done(null, existingUser);
    } catch (error) {
        return done(error);
    }
})

const setStrategies = () => {
    //We say here the strategy we are using
    passport.use('register', registerStrategy);
    passport.use('login', loginStrategy);
    passport.use('logout', loginStrategy);
    // console.log('register ->', registerStrategy)
    // console.log('login ->', loginStrategy)
}

module.exports = {
    setStrategies,
};