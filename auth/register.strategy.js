const LocalStrategy = require('passport-local').Strategy;
//We install bcript to encrypt the passwords
const bcrypt = require('bcrypt');
const User = require('../models/User.model');
const { isValidEmail, isValidPassword, throwError } = require('./utils');


const registerStrategy = new LocalStrategy(
    {
        usernamelField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    async (req, email, password, done) => {
        try {//Check if the user already exists
            const existingUser = await User.findOne({ email });

            if (existingUser) return throwError(400, 'The user already exists', done);

            //Check if the email is val id
            if (!isValidEmail(email)) return throwError(400, 'The email is not valid', done);

            //Check if the password is valid
            if (!isValidPassword(password)) return throwError(400, 'The password must contain 8 characters, 1 uppercase and 1 number', done);

            //Encrypt the password
            const saltRounds = 10;
            const hash = await bcrypt.hash(password, saltRounds);

            //Save usernameField
            const newUser = new User({
                email,
                password: hash,
                name: req.body.name,

            });


            const user = await newUser.save();
            //We add this for security reasons
            user.password = undefined;
            //Return there's no error, but we have a user
            return done(null, user);
        }
        catch (err) {
            return done(err);

        }
    }

);


module.exports = registerStrategy;