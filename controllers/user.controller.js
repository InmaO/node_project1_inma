const User = require('../models/User.model');

const { deleteBooking } = require('../controllers/Booking.controller');

const userGet = async (req, res, next) => {

    try {
        const { id } = req.params;
        const user = await User.findById(id);

        if (user !== null) {
            user.password = undefined;
            return res.status(200).json(user);
        } else {
            throw new Error('[Error] User not found')
        }

    } catch (error) {
        next(error);
    }

}

const userUpdateBooking = async (req, res, next) => {

    try {

        const { userId, bookingId } = req.body;

        const update = {};

        if (bookingId === null || bookingId === undefined) throw new Error('incorrect id');

        const updateUser = await User.findByIdAndUpdate(
            userId,
            { $addToSet: { booking: bookingId } },
            { new: true }
        )

        return res.status(200).json(updateUser);

    } catch (error) {
        return next(error);
    }
}

const userDeleteBooking = async (req, res, next) => {

    try {

        const { userId, bookingId } = req.body;
        const isBookingDeleted = await deleteBooking(bookingId);

        if (isBookingDeleted) {

            const updateUser = await User.findByIdAndUpdate(
                userId,
                { $pull: { booking: bookingId } },
                { new: true }
            )
            updateUser.password = undefined;
            return res.status(200).json(updateUser);

        } else {
            const error = new Error('Failed to delete the booking');
            throw error;
        }

    } catch (error) {
        console.error(error);
        next(error);
    }
}

module.exports = {
    userGet,
    userUpdateBooking,
    userDeleteBooking,
}