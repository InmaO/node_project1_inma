const booking = require("../models/Booking.model");

const bookingGet = async (req, res, next) => {
    try {
        const booking = await booking.find();
        if (booking.length > 0) {
            return res.status(200).json(booking);
        } else {
            const error = new Error('Your booking list is empty');
            throw error;
        }
    } catch (error) {
        return next(error);
    }
};

const bookingGetById = async (req, res, next) => {

    const { id } = req.params;
    try {

        const booking = await booking.findById(id);

        if (bookingGet !== null && booking !== undefined) {

            return res.status(200).json(booking);
        } else {
            const error = new Error('Booking not able to be found');
            error.status = 404;
            throw error;
        }

    } catch (error) {
        return next(error);
    }
};

const bookingPost = async (req, res, next) => {
    try {

        const { hasPaid, day, VIPlounge, extraFacilities } = req.body;

        const newbooking = new booking({
            hasPaid: hasPaid === "false" ? false : true,
            day: new Date(day),
            VIPlounge,
            extraFacilities,

        });
        const createdbooking = await newbooking.save();
        return res.status(200).json(createdbooking);
    } catch (error) {
        console.error(error);
        next(error);
    }
};

const bookingDelete = async (req, res, next) => {
    try {
        const { id } = req.body;

        const bookingDeleted = await booking.findByIdAndDelete(id);

        if (!bookingDeleted) {
            return res.status(404).json('The element does not exist');
        } else {
            return res.status(200).json('Booking is deleted');
        }
    } catch (error) {
        return next(error);
    }
};


module.exports = {
    bookingGet,
    bookingPost,
    bookingDelete,
    bookingGetById,
};