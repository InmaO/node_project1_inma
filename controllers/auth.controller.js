const passport = require('passport');

//This folder is to controll/create all the authentication
const registerPost = (req, res, next) => {

    //We add the const as we have the callback "done" in passport.auth function
    const done = (error, user) => {
        if (error) return next(error);
        // console.log(user)
        req.login(user, (error) => (error ? next(error) : res.json(user)));
    };
    //We add this function "req.logIn();" so the user once has logIn doesn't need to do it again
    // req.logIn();
    passport.authenticate('register', done)(req);
};

const loginPost = (req, res, next) => {
    console.log('loginPost')
    const done = (error, user) => {
        console.log(user)
        if (error) return next(error);


        req.login(user, () => (error ? next(error) : res.json(user)));
    }
    passport.authenticate('login', done)(req);

}

module.exports = {
    registerPost,
    loginPost
}