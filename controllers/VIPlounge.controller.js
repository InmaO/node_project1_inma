const VIPlounge = require("../models/VIPlounge.model");

const VIPloungeGet = async (req, res, next) => {
    try {

        const VIPlounge = await VIPlounge.find();

        if (VIPlounge.length > 0) {
            return res.status(200).json(VIPlounge);
        } else {
            const error = new Error('There is no VIPlounge');
            throw error;
        }
    } catch (error) {
        return next(error);
    }
};


const VIPloungeGetByName = async (req, res, next) => {
    const { name } = req.params;

    try {
        const VIPlounge = await VIPlounge.find({ name: name });

        if (VIPlounge.length > 0) {
            return res.status(200).json(VIPlounge);
        } else {
            const error = new Error('There is no VIPlounge');
            error.status = 404;
            throw error;
        }
    } catch (error) {
        console.error(error);
        return next(error);
    }
};

const VIPloungePost = async (req, res, next) => {
    try {
        const { name } = req.body;

        const newVIPlounge = new VIPlounge(name);

        const createdVIPlounge = await newVIPlounge.save();
        return res.status(200).json(createdVIPlounge);
    } catch (error) {
        console.error(error);
        next(error);
    }
};



const VIPloungeDelete = async (req, res, next) => {
    try {
        const { id } = req.body;
        const VIPloungeDeleted = await VIPlounge.findByIdAndDelete(id);

        if (!VIPloungeDeleted) {
            return res.status(404).json("false");
        } else {
            return res.status(200).json("true");
        }
    } catch (error) {
        return next(error);
    }
};



module.exports = {
    VIPloungeGet,
    VIPloungeGetByName,
    VIPloungePost,
    VIPloungeDelete,
};