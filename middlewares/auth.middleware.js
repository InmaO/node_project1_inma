//If the user is authenticated 
const isAuth = (req, res, next) => {
    return req.isAuthenticated()
        //If the user isn't authenticated will give an error with message
        ? next()
        : res.status(401).json('Hey dude you are not authorized!');
}

//If the user is not an Admin, just have the "user" role
const isAdmin = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.role === 'admin') {
            return next();
        }
        //Gives an error (403 is forbidden) with a message
        return res.status(403).json('Upps! You don not have the power you are a simple mortal!');
    } else {
        //If the user isn't authenticated will give an error with message
        return res.status(401).json('Hey dude you are not authorized!');
    }
}

//We need to export the middleware so we can use them
module.exports = { isAuth, isAdmin };
