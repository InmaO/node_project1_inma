const multer = require('multer');
//We import the libraries that we need for this new function (upload images)
const fs = require('fs');
const path = require('path');

const cloudinary = require('cloudinary').v2;

const ACCEPT_FILES = ['image/jpg', 'image/jpeg', 'image/png'];

const storage = multer.diskStorage({
    fileName: (req, file, callback) => {
        cb(null, Date.now() + file.originalname);
    },

    //With destination we tell where we want to send the info
    destination: (req, file, callback) => {
        const directory = path.join(__dirname, '../public/uploads');
        callback(null, directory);
    },
});

const fileFilter = (req, file, callback) => {
    // console.log(file);
    if (!ACCEPT_FILES.includes(file.mimetype)) {
        const error = new Error('Invalid file type');
        error.status = 400;

        return callback(error, true);
    }
    return callback(null, true);
};

const upload = multer({
    storage,
    fileFilter,
}
);



//This is our middleware to upload files to Cloudinary
const uploadToCloudinary = async (req, res, next) => {
    if (req.file) {
        const filePath = req.file.path;
        //If I have a file, I upload it to Cloudinary
        const image = await cloudinary.uploader.upload(filePath);
        //We add the "file_url" property to our Request
        req.fileUrl = image.secure_url;
        //We erase the local file
        await fs.unlinkSync(filePath);


        return next();
    } else {
        return next();
    }

};

module.exports = {
    upload,
    uploadToCloudinary,
}