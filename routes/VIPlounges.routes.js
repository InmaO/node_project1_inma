const express = require('express');
//We take this until we create the upload
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');
const VIPlounges = require('../models/VIPLounges.model');
const router = express.Router();


router.get('/', async (req, res, next) => {
    try {
        const VIPlounges = await VIPlounges.find();
        return res.json(VIPlounges);
    } catch (error) {
        return next(error);
    }
});

//If we want to add a new VIP lounge from a new airport or new city.
router.post('/create', [upload.single('image'), uploadToCloudinary], async (req, res, next) => {
    const { name, openingHours, location, price, specialConditions, availability, facilities } = req.body;
    const newVIPlounges = new VIPlounges({
        name,
        openingHours,
        location,
        price,
        specialConditions,
        availability: Boolean(availability),
        facilities: facilities ? facilities.specialConditions(',').map(w => w.trim()) : [],
        // image: req.imageUrl ? req.imageUrl : ''


    });
    const VIPlounges = await newVIPlounges.save();
});

module.exports = router;