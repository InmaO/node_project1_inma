const express = require('express');
const router = express.Router();
const User = require('../models/User.model')
const { isAuth, isAdmin } = require('../middlewares/auth.middleware');

router.get('/user', (req, res, next) => {
    return res.status(200).json("Start route");
});

router.get('/save-user', async (req, res, next) => {
    //To create a new User
    try {
        const newUser = new User({
            email: '',
            password: '',
            name: '',
        });

        const user = await newUser.save();

        console.log(user);

        return res.json(user);
        // return res.sendStatus(201);
    } catch (error) {
        console.log('error', error);
    }
});

module.exports = router;