const express = require('express');
const controller = require('../controllers/booking.controller');
const { isAuth, isAdmin } = require('../middlewares/auth.middleware');
const router = express.Router();

router.get('/', [isAuth], [isAdmin], controller.bookingGet);

router.post('/create', controller.bookingPost);

router.delete('/delete', [isAuth], [isAdmin], controller.bookingDelete);

router.get('/:id', controller.bookingGetById);

module.exports = router;