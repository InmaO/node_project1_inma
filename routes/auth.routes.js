//We import express for the router
const express = require('express');
const passport = require('passport');

//We add Router in capital letters as it's a class inside express
const router = express.Router();

router.get('/register', (req, res, next) => {
    return res.render('./auth/register.hbs', { layout: false });
});

router.post('/register', (req, res, next) => {
    const done = (error, user) => {
        if (error) {
            return next(error);
        }
        console.log('User registered -> ', user);

        return res.redirect('/VIPlounges');
    }

    passport.authenticate('register', done)(req);
})

module.exports = router;
