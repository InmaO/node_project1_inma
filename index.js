//express is always required in the 1st line of the code
const express = require('express');
const passport = require('passport');
const session = require('express-session');
const exhbs = require('express-handlebars');
const MongoStore = require('connect-mongo');
// const path = require('path');


const dotenv = require('dotenv');
dotenv.config();

// const path = require('path');
const db = require('./config/db');
//require('./seeds/User.seed.js') Al poner esta función se ejecutará automáticamente cada vez que hagamos cambios. Para evitarlo, la quitamos y cuando lo necesitemos lo ejecutaremos a mano. Lo añadiremos y quitaremos según necesidad
db.connect();

//We import the authentication (auth)
const auth = require('./auth');
auth.setStrategies();

const userRoutes = require('./routes/user.routes');
const authRoutes = require('./routes/auth.routes');
const VIPloungesRoutes = require('./routes/VIPlounges.routes');
const bookingRoutes = require('./routes/booking.routes')
//we add this function "express static" as we want to expose this folder public
//app.use(express.static(path.join(__dirname, 'public')));

const PORT = process.env.PORT || 3000;

const app = express();
app.engine('.hbs', exhbs({ extname: '.hbs' }));
app.set('view engine', '.hbs');

// const router = express.Router();

// app.use((req, res, next) => {
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
//     res.header('Access-Control-Allow-Credentials', true);
//     res.header('Access-Control-Allow-Headers', 'Content-Type');
//     next();
// });

// app.use(express.static(path.join(__dirname, 'public')))

app.use(session(
    {
        secret: 'xckjvhckjbhjkcbhkjcvlkxvjxlkvjx$%&$%!!//',
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 20 * 60 * 1000,
        },
        store: MongoStore.create({ mongoUrl: db.DB_URL })
    }
));

//We call the username middlewares
app.use(passport.initialize());
app.use(passport.session());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use('/', router);
// app.use('/', userRoutes);
app.use('/', userRoutes);
app.use('/auth', authRoutes);
app.use('/VIPlounges', VIPloungesRoutes);
app.use('/booking', bookingRoutes);

// router.get('/', (req, res) => {
//     const myRoutes = [
//         {
//             "route": "/VIPlounges",
//             "description": 'Lounges with their CRUD'
//         },
//         {
//             "route": "/booking",
//             "description": 'Bookings with their CRUD'
//         },

//     ]
//     res.status(200).json(myRoutes);
// });

//This is the route middleware
// app.use('*', (req, res, next) => {
//     const error = new Error('Route not found');
//     error.status = 404;
//     return res.json(error.message);
// })

//This is the error middleware
app.use((error, req, res, next) => {
    return res.json({
        message: error.message || 'Unexpected Error',
        status: error.status || 500
    })
})

app.listen(PORT, () => { console.log(`Service running http://localhost:${PORT}`) });

