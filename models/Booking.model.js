const mongoose = require('mongoose');

const { Schema } = mongoose;

const bookingSchema = new Schema(
    {
        VIPlounge: { type: mongoose.Types.ObjectId, ref: 'VIPlounge' },
        day: { type: Date, required: true },
        extraFacilities: { type: String, required: true },
        hasPaid: { type: Boolean, required: true },

    },
    { timestamps: true }
);

const Booking = mongoose.model('Booking', bookingSchema);

module.exports = Booking;