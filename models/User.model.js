const mongoose = require('mongoose');

// const Schema = mongoose.Schema (esta función es la misma que la línea 4)
const { Schema } = mongoose;

const userSchema = new Schema(
    {
        email: { type: String, required: true },
        password: { type: String, required: true },
        role: { type: String, required: true, enum: ['user', 'admin'], default: 'user' },
        name: { type: String, required: true },
        VIPlounges: [{ type: mongoose.Types.ObjectId, ref: 'Booking' }],
    },
    { timestamps: true }
);

const User = mongoose.model('User', userSchema);

module.exports = User;