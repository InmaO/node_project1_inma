const mongoose = require('mongoose');
const { Schema } = mongoose;

const VIPloungesSchema = new Schema(
    {
        name: { type: String, required: true },
        openingHours: { type: String, required: true },
        location: { type: String, required: true },
        price: { type: Number, required: true },
        specialConditions: { type: String, required: false },
        availability: { type: Boolean, required: true },
        image: { type: String, required: true },
        facilities: [{ type: String }],

    },
    { timestamps: true }
);
const VIPlounges = mongoose.model('VIPlounges', VIPloungesSchema);
module.exports = VIPlounges;