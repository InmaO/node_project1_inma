const mongoose = require('mongoose');
const VIPlounges = require('../models/VIPLounges.model');
const db = require('../config/db');

const VIPlounges = [
    {
        name: 'La Coruña VIP Lounge',
        openingHours: '05:30 - 22:00 Monday - Thursday & Sunday. 05:30 - 21:30 Friday. 05:30 - 21:00 Saturday.',
        location: 'Airside - 1st Floor, Boarding Area. The lounge is located in front of Gates 2-3.',
        price: 15,
        specialConditions: 'Access is permitted 4 hours prior to scheduled flight departure - All Cardholders and guests are required to show a Boarding Pass with confirmed same-day travel for access to the lounge - Children 5 years and under are admitted free - Children under 18 years must be accompanied by an adult - Outside smoking terrace available.',
        availability: true,
        image: 'https://cdn03.collinson.cn/our-lounges/latest-lounges/anf-feb2020-3d1193ef-8e10-4398-92c7-ca290b5c04cb.webp?h=375&la=en&w=500',
        facilities: ['Disabled Access', 'Air Conditioning', 'Internet', 'Refreshments', 'Newspapers/Magazines', 'TV', 'Alcohol', 'Flight Information', 'Digital Card Accepted', 'Wi-Fi'],

    },
    {
        name: 'Barcelona',
        openingHours: '05:45 to the last scheduled flight departure (approx. 21:30) Monday - Friday. 07:00 to the last scheduled flight departure (approx. 21:30) Saturday & Sunday. Due to current travel restrictions impacting the area this lounge is temporarily closed.',
        location: 'Airside - Barcelona-Madrid Shuttle Area. After passing through Security Checks, turn left and proceed to the end of the corridor. T1 Barcelona-Madrid Shuttle flights only.',
        price: 30,
        specialConditions: 'Lounge access is permitted 3 hours prior to a scheduled flight departure - All Cardholders and guests are required to show a Boarding Pass with confirmed same-day travel for access to the lounge and the names must match the Boarding Pass - Guests are limited to one per Cardholder - Lounge access may be restricted due to lounge capacity constraints and the lounge reserves the right to reserve seating as necessary - Children 5 years and under are admitted free - Children under 18 years must be accompanied by an adult - Smart casual dress at all times.',
        availability: false,
        image: 'https://cdn03.collinson.cn/our-lounges/latest-lounges/msy1-feb2020-f3255906-aa36-4063-aae9-81c9cd82c1ed.webp?h=375&la=en&w=500',
        facilities: ['Disabled Access', 'Air Conditioning', 'Shower', 'Internet', 'Refreshments', 'Newspapers/Magazines', 'TV', 'No Smoking', 'Alcohol', 'Flight Information', 'Digital Card Accepted', 'Conference', 'Wi-Fi'],

    },
    {
        name: 'Madrid city Lounge',
        openingHours: '24 hours daily.',
        location: 'Airside - after Passport Control, proceed to the 2nd Floor. The lounge is located between Gates B26 and B29 on the right hand side. Non-Schengen flights.',
        price: 40,
        specialConditions: 'Lounge access is permitted 3 hours prior to a scheduled flight departure - All Cardholders and guests are required to present a valid boarding pass for same day travel - Cardholder name must match the passenger name on the boarding pass - Guests are limited to one per Cardholder and Lounge access may be restricted due to Lounge capacity constraints and the Lounge reserves the right to reserve seating as necessary - Children 5 years and under are admitted free - Children under 18 years must be accompanied by an adult - Outside smoking terrace available.',
        availability: false,
        image: 'https://cdn03.collinson.cn/our-lounges/latest-lounges/ren-feb2020-9bafcb76-0d21-4673-8fdd-82e15c13eddf.webp?h=375&la=en&w=500',
        facilities: ['Disabled Access', 'Air Conditioning', 'Internet', 'Refreshments', 'Newspapers/Magazines', 'TV', 'No Smoking', 'Alcohol', 'Flight Information', 'Digital Card Accepted', 'Wi-Fi', 'Telephone', 'Fax'],

    },
    {
        name: 'The Valencian lounge',
        openingHours: '05:00 - 22:00 daily.',
        location: 'Airside - after Security Checks, Level 1. The lounge is located in the main departure lounge between Terminals 1 and R, close to Gate 12.',
        price: 15,
        specialConditions: 'Access is permitted 4 hours prior to scheduled flight departure - The lounge is only accessible upon presentation of a valid Boarding Pass for same day travel - Children 5 years and under are admitted free - Children under 18 years must be accompanied by an adult - Separate children´s play area available.',
        availability: true,
        image: 'https://cdn03.collinson.cn/our-lounges/latest-lounges/svo15-feb2020-4a737896-0263-41c0-86bb-2a24aea98ac3.webp?h=375&la=en&w=500',
        facilities: ['Disabled Access', 'Air Conditioning', 'Internet', 'Refreshments', 'Newspapers/Magazines', 'No Smoking', 'Alcohol', 'Digital Card Accepted', 'Wi-Fi'],

    },
];
/**
 * Process to save the initial data in our db;
 * 1. Buscar si la colección existe o no.
 *     1.1: Si existe la colección, la borramos y la creamos de nuevo
 *     1.2: Si no existe, la creamos directamente.
 */

//With this function we connect to the db. Me devolverá un promesa y para ello utilizamos "then". Como la función es muy larga, utilizamos programación funcional y la desencademos. En cada línea ponemos una función
mongoose.
    connect(db.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(async () => {

        const allVIPlounges = await VIPlounges.find(); //esto hace una búsqueda en la bd y nos devuelve todo lo que tiene

        if (allVIPlounges.length) {
            console.log(`[Find]: Found ${allVIPlounges.length} lounges`);
            await VIPlounges.collection.drop();
            console.log("[Delete]: Collection  (VIP lounge) removed successfully");
        } else {
            console.log('[Find: Users not found')
        }
    })
    .catch(error => console.log('[Error]: Deleting the db', error))
    .then(async () => {
        await VIPlounges.insertMany(VIPlounges);
        console.log('[Success]: Successfully added VIP lounges');
    })
    .catch(error => console.log('[Error]: Adding VIP lounges', error))
    .finally(() => mongoose.disconnect());
