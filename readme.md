# Comandos abreviados npm

```
npm i -> npm install --save [nombrePaquete]
npm i -D -> npm install --save-dev
npm rm -> npm remove [nombrePaquete]
```


# Cómo crear un proyecto de NPM
1. `git init``
2.  crear `.gitignore`y crear file node_modules
3. `npm init -y`
4. `npm i express`
5. Crear el código básico de nuestro servidor
```js
const express = require('express');

const PORT = 3000; 
const app = express();
app.listen(PORT, () => {
    console.log(`Servidor funcionando en http://localhost:${PORT}`);
});
```
6. Script "start" en package.json
7. Crear rutas
```js
const app = express();

const router = express.Router();

router.get('/', (req, res) => {
    return res.send('Hola clase!');
});

router.get('/movies', (req, res) => {
    const movies = ['Spiderman', 'Taxi Driver', 'Tenet'];

    return res.send(movies);
});

app.use('/', router); // Le decimos a express que "use" el router que hemos configurado

app.listen(PORT, () => {
    console.log(`Servidor funcionando en http://localhost:${PORT}`);
});
````
8. 