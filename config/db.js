const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const DB_URL = process.env.DB_URL || 'mongodb://localhost:27017/node_project1_inma';

const connect = async () => {
    try {
        const db = await mongoose.connect(DB_URL, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useFindAndModify: false,
        });

        const { name, host } = db.connection;
        console.log(`Successfully connected to ${name} in ${host}`);
    } catch (error) {
        console.log(`An error has occurred connected to the DB ${error}`);
    }
}

module.exports = {
    connect,
    DB_URL,
};
